fix_PG_sequences_ownership.sql
------------------------------

This script makes sure all sequences in a PostgreSQL database belong to a
column. Tha happens if you create the sequence after (not using the pseudo type
SERIAL) and forget to set the OWNED BY. Some script that automatically migrate
from one SGDB to another may do that.

It was needed once when I had to port a massive MySQL DB to PG.

