DROP VIEW IF EXISTS orphaned_sequences;                                           
CREATE VIEW orphaned_sequences AS                                                 
    WITH all_seqs (sequence_name, associated_column, need_fix) AS (               
        SELECT replace(substring(column_default,'''.*'''),'''','') AS sequence_name, 
               concat(table_schema,'.',table_name,'.',column_name)::text AS associated_column,
               pg_get_serial_sequence(table_schema || '.' || table_name, column_name) IS NULL AS need_fix
        FROM information_schema.columns                                           
        WHERE table_schema NOT IN ('information_schema', 'pg_catalog')            
          AND column_default LIKE 'nextval%')                                     
    SELECT                                                                        
        sequence_name,                                                            
        associated_column,                                                        
        'ALTER SEQUENCE ' || sequence_name || ' OWNED BY ' || associated_column || ';' AS query_to_fix
    FROM all_seqs WHERE need_fix IS TRUE;                                         
                                                                                  
DROP FUNCTION IF EXISTS fix_orphan_sequences();                                   
CREATE FUNCTION fix_orphan_sequences() RETURNS TABLE (left_to_fix TEXT)           
LANGUAGE plpgsql                                                                  
AS $$                                                                             
    DECLARE row record;                                                           
BEGIN                                                                             
    FOR row IN SELECT * from orphaned_sequences                                   
    LOOP                                                                          
        RAISE INFO 'Associating [''%''] TO [''%'']', row.sequence_name, row.associated_column;
        EXECUTE row.query_to_fix;                                                                                                                              
    END LOOP;                                                                     
    RETURN QUERY SELECT sequence_name  FROM orphaned_sequences ;                  
END $$;                                                                           
